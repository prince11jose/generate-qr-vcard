import qrcode

# Define vCard info
first_name = "Prince"
last_name = "Jose"
phone_number = "+91-866-707-8166"
email = "prince@movingwalls.com"
company = "Moving Walls India Pvt Ltd"
title = "DevOps Engineer"

vcard = f"BEGIN:VCARD\nVERSION:3.0\n\
N:{last_name};{first_name};;;\n\
FN:{first_name} {last_name}\n\
TEL;TYPE=work,voice;VALUE=uri:tel:{phone_number}\n\
EMAIL:{email}\n\
ORG:{company}\n\
TITLE:{title}\n\
END:VCARD"
qr = qrcode.QRCode(
    version=None,
    box_size=10,
    border=4,
)
qr.add_data(vcard)
qr.make(fit=True)
img_qr = qr.make_image(fill_color="black", back_color="white")
img_qr.save(f"{first_name}-vcard_qr_without_logo.png")
